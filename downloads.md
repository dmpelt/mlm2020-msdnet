# Downloads

Put these all in the same folder:

* Code runs in Python 3, using anaconda:

[https://www.anaconda.com/distribution/](https://www.anaconda.com/distribution/)

Note: there is also a PyTorch implementation written by someone in our group:

[https://github.com/ahendriksen/msd_pytorch](https://github.com/ahendriksen/msd_pytorch)

* Data for denoising (download ‘map (gz)’ under 'downloads'):

[https://www.ebi.ac.uk/pdbe/entry/emdb/EMD-4603/](https://www.ebi.ac.uk/pdbe/entry/emdb/EMD-4603/)

After download, extract (for example, using 7-Zip)

* Data for segmentation (download all 'sub-volumes' below the images):

[https://www.epfl.ch/labs/cvlab/data/data-em/](https://www.epfl.ch/labs/cvlab/data/data-em/)

* Scripts:

[https://gitlab.com/dmpelt/mlm2020-msdnet/-/archive/master/mlm2020-msdnet-master.zip](https://gitlab.com/dmpelt/mlm2020-msdnet/-/archive/master/mlm2020-msdnet-master.zip)
