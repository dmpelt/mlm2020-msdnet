import numpy as np
import skimage.transform as skt
import os
import tifffile
import imageio

def read_data():
    # https://www.epfl.ch/labs/cvlab/data/data-em/
    # Read 3D images from TIFF files, and crop to make divisible by 3
    rec = tifffile.imread('training.tif')[:,:,:-1]
    seg = tifffile.imread('training_groundtruth.tif')[:,:,:-1]
    # Reduce size to make tutorial faster
    rec = skt.downscale_local_mean(rec, (1,3,3))
    seg = seg[:,1::3,1::3]
    return rec, seg

# Read images and segmentation from file
rec, seg = read_data()

# Output slices as TIFF
os.makedirs('reconstruction',exist_ok=True)
os.makedirs('segmentation',exist_ok=True)
for i in range(rec.shape[0]):
    imageio.imsave('reconstruction/{:04d}.tiff'.format(i), rec[i].astype(np.float32))
    imageio.imsave('segmentation/{:04d}.tiff'.format(i), seg[i].astype(np.uint8))
