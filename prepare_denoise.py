import numpy as np
import skimage.transform as skt
import os
import imageio

def read_data():
    # https://www.ebi.ac.uk/pdbe/entry/emdb/EMD-4603/
    # Read data from MRC file
    f = open('emd_4603.map','rb')
    dims = np.fromfile(f, dtype=np.dtype('u4'), count=3)
    tpe = np.fromfile(f, dtype=np.dtype('u4'), count=1)[0] 
    f.seek(23 * 4) 
    nxt = np.fromfile(f, dtype=np.dtype('u4'), count=1)[0] 
    f.seek(1024 + nxt)
    rec = np.fromfile(f, dtype=np.dtype('i2'), count=dims[0]*dims[1]*dims[2]).reshape((dims[2],dims[1],dims[0]))  
    f.close()
    # Crop relevant part of data, and apply padding to make it divisible by three
    return np.pad(rec[150:300],((0,0),(1,1),(1,1)),mode='edge')

rec = read_data()

# Reduce size of data to make tutorial faster
# Use 3x3x3 mean filter to create 'high-dose' images
high_dose_rec = skt.downscale_local_mean(rec,(3,3,3))

# Only take every 3rd pixel to create 'low-dose' images
low_dose_rec = rec[1::3,1::3,1::3]

# Output slices as TIFF
os.makedirs('low_dose',exist_ok=True)
os.makedirs('high_dose',exist_ok=True)
for i in range(high_dose_rec.shape[0]):
    imageio.imsave('low_dose/{:04d}.tiff'.format(i), low_dose_rec[i].astype(np.float32))
    imageio.imsave('high_dose/{:04d}.tiff'.format(i), high_dose_rec[i].astype(np.float32))
