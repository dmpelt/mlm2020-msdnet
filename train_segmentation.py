import msdnet
from pathlib import Path

# Set dilations to [1,5] (in paper, we use [1,10], but here we have small images)
dilations = msdnet.dilations.IncrementDilations(5)

# Create MSD network with 100 layers, 1 input image and 2 target images (one for each label)
# Set gpu=False to use CPU
n = msdnet.network.SegmentationMSDNet(100, dilations, 1, 2, gpu=True)

# Initialize network variables randomly
n.initialize()

# Define reconstruction images as input, and segmneted images as target
flsin = sorted((Path('reconstruction')).glob('*.tiff'))
flstg = sorted((Path('segmentation')).glob('*.tiff'))
dats = []
datsv = []
for i in range(len(flsin)):
    # Tell code that data is loaded from TIFF files
    d = msdnet.data.ImageFileDataPoint(str(flsin[i]),str(flstg[i]))
    # Tell code that the labels we want to find have pixel values 0 and 255
    d_oh = msdnet.data.OneHotDataPoint(d, [0,255])
    # Put every 5th slice in the validation set, and rest in training set
    if i%10!=5:
        # Augment training data by rotating and flipping
        d_augm = msdnet.data.RotateAndFlipDataPoint(d_oh)
        dats.append(d_augm)
    else:
        datsv.append(d_oh)

# Normalize network input and targets
n.normalizeinout(dats)

# Use a batch size of 1 image
bprov = msdnet.data.BatchProvider(dats,1)

# Validate using the mean-squared error
val = msdnet.validate.MSEValidation(datsv)

# Train using the ADAM algorithm with default parameters
t = msdnet.train.AdamAlgorithm(n)

# Log validation error to console, file, and image files
consolelog = msdnet.loggers.ConsoleLogger()
filelog = msdnet.loggers.FileLogger('log_segm.txt')
imagelog = msdnet.loggers.ImageLabelLogger('log_segm', onlyifbetter=True)
singlechannellog = msdnet.loggers.ImageLogger('log_segm_singlechannel', chan_out=1, onlyifbetter=True)

# Start training. This call stops after 100 non-improving validation steps, but can also be killed in the terminal
msdnet.train.train(n, t, val, bprov, 'segm_params.h5',loggers=[consolelog,filelog,imagelog,singlechannellog], val_every=len(datsv),progress=True, stopcrit=100)
