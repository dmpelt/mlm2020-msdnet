import msdnet
from pathlib import Path
import numpy as np
import pyqtgraph as pq

# Set dilations to [1,5] (in paper, we use [1,10], but here we have small images)
dilations = msdnet.dilations.IncrementDilations(5)

# Create MSD network with 100 layers, 1 input image and 1 target image
# Set gpu=False to use CPU
n = msdnet.network.MSDNet(100, dilations, 1, 1, gpu=True)

# Initialize network variables randomly
n.initialize()

# Define low-dose images as input, and high-dose images as target
flsin = sorted((Path('low_dose')).glob('*.tiff'))
flstg = sorted((Path('high_dose')).glob('*.tiff'))
dats = []
datsv = []
for i in range(len(flsin)):
    # Tell code that data is loaded from TIFF files
    d = msdnet.data.ImageFileDataPoint(str(flsin[i]),str(flstg[i]))
    # Put every 5th slice in the validation set, and rest in training set
    if i%10!=5:
        # Augment training data by rotating and flipping
        d_augm = msdnet.data.RotateAndFlipDataPoint(d)
        dats.append(d_augm)
    else:
        datsv.append(d)

# Normalize network input and targets
n.normalizeinout(dats)

# Use a batch size of 1 image
bprov = msdnet.data.BatchProvider(dats,1)

# Validate using the mean-squared error
val = msdnet.validate.MSEValidation(datsv)

# Train using the ADAM algorithm with default parameters
t = msdnet.train.AdamAlgorithm(n)

# Log validation error to console and file
consolelog = msdnet.loggers.ConsoleLogger()
filelog = msdnet.loggers.FileLogger('log_regr.txt')

# Just for the tutorial, show images during training using PyQTGraph
class TempLogger(msdnet.loggers.Logger):
    def initialize(self):
        self.im = pq.image()

    def makelog(self, v):
        if v.curerr <= v.best:
            inp = datsv[len(datsv)//2].input
            tar = datsv[len(datsv)//2].target
            out = n.forward(inp)
            ims = np.vstack([inp[0], out[0], tar[0]])
            self.im.setImage(ims,levels=(-20,20))
            pq.QtGui.QApplication.processEvents()


# Start training. This call stops after 100 non-improving validation steps, but can also be killed in the terminal
msdnet.train.train(n, t, val, bprov, 'regr_params.h5',loggers=[consolelog,filelog,TempLogger()], val_every=len(datsv), progress=True, stopcrit=100)
