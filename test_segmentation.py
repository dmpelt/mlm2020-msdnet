import numpy as np
import skimage.transform as skt
import os
import tifffile
import pyqtgraph as pq
import msdnet

def read_data():
    # https://www.epfl.ch/labs/cvlab/data/data-em/
    # Read 3D images from TIFF files, and crop to make divisible by 3
    # Important: make input images as similar as possible to training images
    # In this case: process in exactly the same way as training images
    rec = tifffile.imread('testing.tif')[:,:,:-1]
    seg = tifffile.imread('testing_groundtruth.tif')[:,:,:-1]
    rec = skt.downscale_local_mean(rec, (1,3,3))
    seg = seg[:,1::3,1::3]
    return rec, seg

# Read images and segmentation from file
rec, seg = read_data()

# Load trained network from file
n = msdnet.network.SegmentationMSDNet.from_file('segm_params.h5')

# Process every slice of the test stack
nn_seg = np.zeros_like(rec)
for i in range(rec.shape[0]):
    # Apply trained network to current slice
    # and save probabilty map of 1st label
    nn_seg[i] = n.forward(rec[i:i+1])[1]

# The rest is to visualize results on screen
rec = (rec - rec.min())/(rec.max()-rec.min())
vis = np.zeros((rec.shape[0],rec.shape[1]*3,rec.shape[2]), dtype=np.float32)
vis[:,:rec.shape[1],:] = nn_seg
vis[:,rec.shape[1]:-rec.shape[1],:] = rec
vis[:,-rec.shape[1]:,:] = seg>0
pq.image(vis)
input()