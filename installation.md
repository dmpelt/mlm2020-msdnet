# Installation

* Install anaconda (OS specific)

For linux: `bash Anaconda3....sh`

* Install MSDNet code inside a new conda environment:

`conda create -n mlm2020msd -c defaults -c conda-forge python=3.7 msdnet pyqtgraph tifffile scikit-image`

* Switch to new environment:

`conda activate mlm2020msd`

* Test whether install was successful:

`python -c 'import msdnet'`

On OSX, you might need to run it like this instead:

`KMP_DUPLICATE_LIB_OK=TRUE python3 -c 'import msdnet'`